/*Program to find Palindromes in between a string*/

import java.util.*;

class MaximumPalindromes
{
	
	/*Method to reverse a string and 
	* check if it is palindrome or not*/
	
	void  checkPalin(String s)
	{
		String rev=new StringBuffer(s).reverse().toString();
		if(rev.equals(s))
		{		
			System.out.print(s + " / ");
		}
	}
	
	public static void main(String args[])
	{
		
		/*Create a class object to call checkPalin() method*/
		
		MaximumPalindromes mp=new MaximumPalindromes();
		
		/*Declare variables for storing 
		* input string,start and end value*/
		
		String s="",substr="";
		int start,end;
		Scanner sc=new Scanner(System.in);
		
		/*Take input string,start value and end value from user*/
		
		System.out.println("Enter a String?");
		s=sc.next();
		System.out.println("Enter Start Position?");
		start=sc.nextInt();
		System.out.println("Enter End position?");
		end=sc.nextInt();
		
		/*a substr named string variable stores
		* a sub-part of string that starts from 
		* start value upto end value*/
		
		substr=s.substring(start-1, end);
		System.out.println("Possible palindromes from " + substr + " are !!!");
		mp.checkPalin(substr);
		
		/*Take a single character from substr and 
		* check for palindrome, do the same for 
		* two characters, three characters and so on
		* upto the length of substr*/
		
		for(int i=0;i<=substr.length();i++)
		{
			for(int j=i;j<substr.length();j++)
			{
				String sub=substr.substring(i,j+1);
				mp.checkPalin(sub);
			}
		}
	}		
}