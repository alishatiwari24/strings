/*Program to Check if two Strings are equal or not*/

import java.util.*;
class StringEquality
{
	public static void main(String args[])
	{
		/*Declare & initialize variables s1 & s2
		* to store string, flag as temporary variable*/
		
		String s1,s2;
		char ch1,ch2;
		int flag=0;
		
		/*Getting two strings from user s1 & s2*/
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter First String?");
		s1=sc.next();
		System.out.println("Enter Second String?");
		s2=sc.next();
		
		/*Comparing length of s1 & s2*/
		
		if(s1.length()==s2.length())
		{
			
			/*If both s1 & s2 then compare 
			* both character by character*/
			
			for(int i=0;i<s1.length();i++)
			{
				if(s1.charAt(i)==s2.charAt(i))
				{
					flag++;
				}
			}
		}
		
		/*Printing Results*/
		
		if(flag==s1.length())
		{
			System.out.println("Strings are Equal!!!");
		}
		else
		{
			System.out.println("Strings are Not Equal!!!");
		}
	}
}