/*Program to find if a string is Palindrome or not*/

import java.util.Scanner;

class StringPalindrome
{
	public static void main(String args[]) throws Exception 
	{
		/*declare and initialize variable for
		 * storing string,reverse
		 *string,length & a temporary variable flag
		 **/
		
		String s1="",s2="";
		int len,flag=0;
		
		/*Take input String s1 from user */
		
		System.out.println("Enter a string?");
		Scanner sc=new Scanner(System.in);
		s1=sc.next();
		len=s1.length();
		
		/*Reverse string & store in string variable s2*/
		
		for(int i=len-1;i>=0;i--)
		{
			s2+=s1.charAt(i);
		}
		
		/*Compare s1 & s2 character by character*/
		
		for(int i=0;i<len;i++)
		{
			if(s1.charAt(i)==s2.charAt(i))
			{
				/*if both s1 & s2 are same then flag is set to 1*/
				flag++;
			}
		}
		
		/*Print the result*/
		
		if(flag==s1.length())
		{
			System.out.println("String is Palindrome!!!");
		}
		else
		{
			System.out.println("String is Not Palindrome!!!");
		}
		
	}
}
