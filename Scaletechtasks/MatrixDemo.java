/*Program to find if a given submatrix is a subset of mainmatrix*/

import java.util.*;

class MatrixDemo
{	
	public static void main(String args[])
	{
		
		Scanner sc=new Scanner(System.in);
		
		/*Declaring mainmatrix[3][3] for storing 3*3 matrix
		* and submatrix[2][2] for storing submatrix of 2*2 size*/
		
		int[][] mainmatrix=new int[3][3];
		int[][] submatrix=new int[2][2];
		
		/*Taking mainmatrix from user*/
		
		System.out.println("Enter main matrix?");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				mainmatrix[i][j]=sc.nextInt();
			}
		}
		
		/*Displaying mainmatrix that user have entered */
		
		System.out.println("Main Matrix is!!! ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(mainmatrix[i][j]+ " ");
			}
			System.out.println("");
		}
		
		/* Taking submatrix from user */
		
		System.out.println("Enter sub matrix?");
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				submatrix[i][j]=sc.nextInt();
			}
		}
		
		/*Displaying submatrix that user have entered */
		
		System.out.println("Sub Matrix is!!! ");
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				System.out.print(submatrix[i][j] + " ");
			}
			System.out.println("");
		}
		
		/*A temporary variable flag is initialized to 0 ,
		* it is set to 1 if submatrix is a subset of mainmatrix*/
		
		int flag=0;
		
		/*Variable k is used as indexes of submatrix*/
		
		int k=0;
		
		/*Comparing submatrix values with mainmatrix value
		 * if first and second value of a row are same, then
		 * compare first and second value of next row,
		 * if all four values are same then set flag to 1*/
		
		for(int i=1;i<=3;i++)
		{
			for(int j=1;j<=3;j++)
			{	
				if(submatrix[k][k]==mainmatrix[i-1][j-1] && submatrix[k][k+1]==mainmatrix[i-1][j])
				{	
					if(submatrix[k+1][k]==mainmatrix[i][j-1] && submatrix[k+1][k+1]==mainmatrix[i][j])
					{
						flag=1;
					}	
				}
			}
		}	
		
		/*If flag variable is set to 1 then is a 
		 * Subset of mainmatrix otherwise not */
		
		if(flag==1)
		{
			System.out.println("Sub Matrix is Subset of Main matrix!!!");
		}
		else
		{
			System.out.println("Sub Matrix is Not Subset of Main matrix!!!");
		}
	
	}
}