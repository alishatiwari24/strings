import java.util.*;
class MatrixDemo
{
	
	public static void main(String args[])
	{
		
		Scanner sc=new Scanner(System.in);
		int[][] mainmatrix=new int[3][3];
		int[][] submatrix=new int[2][2];
		System.out.println("Enter main matrix?");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				mainmatrix[i][j]=sc.nextInt();
			}
		}
		System.out.println("Main Matrix is!!! ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(mainmatrix[i][j]+ " ");
			}
			System.out.println("");
		}
		System.out.println("Enter sub matrix?");
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				submatrix[i][j]=sc.nextInt();
			}
		}
		System.out.println("Sub Matrix is!!! ");
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				System.out.print(submatrix[i][j] + " ");
			}
			System.out.println("");
		}
		int flag=0;
		int main2[][]=new int[2][2];
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				main2[i][j]=mainmatrix[i][j+1];
			}
		}
		int main3[][]=new int[2][2];
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				main3[i][j]=mainmatrix[i+1][j];
			}
		}
		int main4[][]=new int[2][2];
		for(int i=0;i<2;i++)
		{
			for(int j=0;j<2;j++)
			{
				main4[i][j]=mainmatrix[i+1][j+1];
			}
		}
		int k=0;
		for(int i=1;i<=3;i++)
		{
			for(int j=1;j<=3;j++)
			{	
				if(submatrix[k][k]==mainmatrix[i-1][j-1] && submatrix[k][k+1]==mainmatrix[i-1][j])
				{	
					if(submatrix[k+1][k]==mainmatrix[i][j-1] && submatrix[k+1][k+1]==mainmatrix[i][j])
					{
						flag=1;
					}	
				}
			}
		}	
		if(flag==1)
		{
			System.out.println("Sub Matrix is Subset of Main matrix!!!");
		}
		else
		{
			System.out.println("Sub Matrix is Not Subset of Main matrix!!!");
		}
	
	}
}