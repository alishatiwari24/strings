/*Program to remove duplicates from a given string*/

import java.util.*;

class DuplicateRemove
{
	public static void main(String args[])
	{
		/*Declare and initialize variables*/
		String s="",rep="";
		char ch;
				
		/*Take a input string from user*/
				
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a String?");
		s=sc.next();
		System.out.println("Entered String : "+s);
				
		/*Take first character from String s and
		*check if it is present in new string rep
		*if it is not present then insert it in 
		*new string otherwise fetch next character 
		*and repeat the process*/
				
		for(int i=0;i<s.length();i++)
		{
			ch=s.charAt(i);
			if(rep.indexOf(ch)==-1)
			{
				rep+=ch;
			}
		}
				
		/*Display string that contains unique characters*/
				
		System.out.println(rep);
	}
}