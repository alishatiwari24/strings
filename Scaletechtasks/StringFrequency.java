/*Program to check if a string has equal no of frequency of alphabets*/

import java.util.Scanner;

class StringFrequency
{
	/*Method to count no. of times a character appears*/
	
	int countChar(String st,char ch)
	{
		int count=0;
		for(int i=0;i<st.length();i++)
		{
			if(ch==st.charAt(i))
			{
				count++;
			}
		}
		return count;
	}
	public static void main(String args[])
	{
		/*Create a class object to call countChar() method,
		 * declare variable s to store string
		 * take a string as input from user */
		
		StringFrequency sf=new StringFrequency();
		String s="";
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a String?");
		s=sc.next();
		int flag=0;
		
		/*c[] is a character array that stores 
		 * no. of counts for each alphabet*/
		
		int c[]=new int[s.length()];
		for(int i=0;i<s.length();i++)
		{
			c[i]=sf.countChar(s, s.charAt(i));
		}
		
		/*Compare current alphabet's no. of count 
		 * with next alphabet, if both are same then 
		 * increment flag by 1 */
		
		for(int i=0;i<c.length-1;i++)
		{
			if(c[i]==c[i+1])
			{
				flag++;
			}
		}
		
		/*If all alphabets have same frequency, 
		 * diaplay appropriate result*/
		
		if(flag==s.length()-1)
		{
			System.out.println("All alphabets have Equal Frequency!!!");
		}
		else
		{
			System.out.println("All alphabets have Different Frequencies!!!");
		}
	}
}